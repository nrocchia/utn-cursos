﻿using Microsoft.AspNetCore.Mvc;

namespace UTN_SEU_Cursos.Controllers
{
    public class CursosController : Controller
    {

        public IActionResult index()
        {
            return View("Index");
        }
        public IActionResult online()
        {
            return View("Online");
        }
        public IActionResult streaming()
        {
            return View("Streaming");
        }
        public IActionResult presencial()
        {
            return View("Presencial");
        }

        public IActionResult automotor()
        {
            return View("Automotor");
        }

        public IActionResult motos()
        {
            return View("Motos");
        }

        public IActionResult communityManager()
        {
            return View("communityManager");
        }

        public IActionResult refrigeracion()
        {
            return View("Refrigeracion");
        }
    }
}
